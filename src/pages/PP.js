import React from "react";
import Header from './Header';
import TopBar from './topbar';
import NavBar from './navbar';

class PP extends React.Component {
    render() {
        return (
        	<div>
        		<Header />
				<TopBar />
        		<h1> Privacy Policy </h1>
        		<p> This site using the domain cfotsl.org is controlled by the Children's Foundation of Technology. 
        		CFOTSL provides to you, a website user, complimentary information service, at no charge, offered by the Children's Foundation of Technology- Sierra Leone with the express condition that users agree to be bound by the terms and conditions set forth below.
        		</p>

                <p> The Children’s Foundation of Technology- Sierra Leone respects your privacy and preferences. This privacy policy (the “Privacy Policy”) sets forth the terms under which you share your personal data with the Children’s Foundation of Technology, how the Children’s Foundation of Technology collects and uses your data, and your rights with respect to understanding and modifying how it will be collected and used. </p>
                <p> Generally: This Privacy Policy applies to the personal data provided to the Children’s Foundation of Technology when you make a donation, register for an event, sign up for a newsletter, purchase a product, or otherwise provide us with information via the Children’s Foundation of Technology, which is located at __________________ or any other domain names owned or controlled by the Children’s Foundation of Technology (the “Website”). It also applies to personal data you may provide to the Children’s Foundation of Technology via telephone, email, regular mail, social media, texting, mobile applications, at a special event, in response to a solicitation or face-to-face.
Minors: It is the Children’s Foundation of Technology’s policy not to solicit knowingly any personally identifiable information from children under the age of 18. Children under 18 are not authorized to make a donation or purchase, sign up for an event or program, or otherwise provide any personally identifiable information without consent from a parent or legal guardian. </p>
                <p> Outside the U.S. and Canada: Most country-level Children’s Foundation of Technology organizations are legally independent of the Children’s Foundation of Technology, governed by their own boards of directors who are responsible for the collection and use of personal data that you may provide.

Sharing of information within the Children’s Foundation of Technology network: When you provide personal data to a National Organization in connection with your participation in a Global Village build or other international volunteer event that the Children’s Foundation of Technology sponsors, your personal data may be shared between (i) the Receiving Organization t and (ii) the Children’s Foundation of Technology. Unless you inform the Children’s Foundation of Technology or the Receiving Organization that you do not consent to marketing by Hosting Organization, the Hosting Organization may also contact you about its own programs and activities in the future.
 </p>
                <p> The Children’s Foundation of Technology may request from you, or you may volunteer to provide, your contact information, including your name, mailing address, phone number(s), social media handles and email address(es). We hold and process supporters' personal data for a number of reasons: </p>
                <ul> 
                    <li> To keep a record of donations made and actions taken by our supporters and our communications with them </li>
                    <li> To send our supporters marketing information about our projects, fundraising activities and appeals where we have their consent or are otherwise permitted to do so </li>
                    <li> To fulfil contractual obligations entered into with supporters </li>
                    <li> To support volunteers, such as during build or fundraising events </li>
                    <li> To support community based fundraising and campaigning </li>
                    <li> To ensure we do not send unwanted information to supporters or members of the public who have informed us they do not wish to be contacted </li>
                    <li> Manage supporters’ accounts and provide customer service. </li>
                    <li> Enforce the Website Terms of Service. </li>
                    <li> Perform other functions as described at the time the Children’s Foundation of Technology collects information. </li>
                </ul>
                <p> If you make a donation or a purchase with the Children’s Foundation of Technology, or otherwise provide us your information, the Children’s Foundation of Technology may contact you from time to time about opportunities to make additional donations or purchases or to provide you information about upcoming programs. </p>
                <p> Unless we are informed that we do not have consent (see guidelines below for modifying your consent), the Children’s Foundation of Technology may also share the data of its U.S. and Canadian resident supporters with select third-party sponsors, service providers and other organizations with which the Children’s Foundation of Technology’s partners, whose products and services may be of interest to those supporters. We may also provide our U.S. and Canadian resident supporters with information about services from third parties. See guidelines for modifying your consent (below, at “How to control what we send you or update your personal information”). </p>
                <p> The Children’s foundation of Technology will contact you for marketing purposes—for example, to keep you up to date on our work, or let you know how you can support that work—only where we have your consent or we are otherwise allowed to do so because of your prior engagement with the Children’s Foundation of Technology, as explained further below.
We will make it easy for you to tell us if you would like to receive marketing communications from us and hear more about our work and the ways in which you would like to receive this information. We will not send you marketing material if you tell us that you do not wish to receive it. Instructions for how to do so are below.
 </p>
                <p> Duration: Where you give us your consent to send marketing information, we will wherever possible let you know how long this consent will last. The Children’s Foundation of Technology will presume your consent to last for 24 months, unless you withhold consent to this duration. After this time, in order for us to continue to update you, the Children’s Foundation of Technology will seek your refreshed consent. You can update or withdraw your consent at any time. </p>
                <p> The Children’s Foundation of Technology will presume a longer period of consent in several exceptions: </p>
                <ul>
                    <li> Where you have commited to making a regular (for example, monthly) donation. </li>
                    <li> In this situation, and unless you withdraw your consent, we will treat consent as lasting until you cancel your donation, at which point your consent will expire 24 months after the last donation. This period allows the Children’s Foundation of Technology to keep you up to date with the impact of your contributions, and to ask whether alternative support would be of interest. </li>
                    <li> Where you have notified us that you will be leaving a legacy to the Children’s Foundation of Technology. </li>
                </ul>
                <p> Our Reliance on Your Prior Support: You may also receive marketing information from the Children’s Foundation of Technology if you have previously made a similar donation to, or have previously purchased similar goods and services from the Children’s Foundation of Technology. However, we will not rely on your prior support as the basis for our continued marketing to you if you have opted out of receiving emails, newsletters, or other marketing materials in your communications to the Children’s Foundation of Technology. </p>
                <p> The Children’s Foundation of Technology may obtain your personal data in the following circumstances:  </p>
                <ul> 
                    <li> When you give it to the Children’s Foundation of Technology </li>
                        <p> We will obtain your personal data directly when you make a donation, sign up for one of our events, purchase products from the Children’s Foundation of Technology online retail store, or when you communicate with us directly in some other way. </p>
                    <li> When you give it to the Children’s Foundation of Technology indirectly </li>
                        <p> We will obtain your personal data when you communicate to your employer or to a retail partner of the Children’s Foundation of Technology during your point of purchase that you affirmatively designate the Children’s Foundation of Technology to receive a personal contribution from you.
Sometimes your personal data is collected by an organization working on the Children’s Foundation of Technology’s behalf (for example, a professional fundraising agency). In such cases, the agency is acting on our behalf, and we are the "data controller" responsible for the security and proper processing of your data, just as if you had given it to Children's Foundation of Technology directly.
 </p>
                    <li> When you access the Children’s Foundation of Technology’s sponsored social media </li>
                        <p> We might also obtain your personal data through your use of social media such as Facebook, Twitter or YouTube, depending on your settings or the privacy policies of these social media and messaging services. To change your settings on these services, please refer to their privacy notices, which will tell you how to do this. </p>
                    <li> When the information is publicly available </li>
                        <p> We might also obtain personal data about individuals who may be interested in giving major gifts to charities or organizations like the Children’s Foundation of Technology. In these cases, we may seek to find out more about these individuals’ interests and motivations for giving through publicly available information. The information sources may include newspaper or other media coverage, open postings on social media sites such as Facebook, and services that aggregate data on charitable giving. The Children’s Foundation of Technology will not retain publicly available data relating to major donors without their consent, which will be sought at the earliest practical opportunity. </p>
                    <li> When we use cookies </li>
                        <p> Cookies are a useful way for us to understand how supporters use the Children’s Foundation of Technology’s website. When you visit our website, we will use cookies to collect data from your computer or other device, such as a smartphone or tablet. Cookies are created by your web browser when you visit our website. Every time you return to our website, your browser will send the cookie file back to the website's server. They improve your experience of using our website by, for example, preserving your preference settings so that you are shown information likely to be most relevant to you, and measuring your use of the website so that we may continuously improve it. Cookies can also be used to show you relevant Children’s Foundation of Technology content on social media services.  </p>
                </ul>
                <p> The Children's Foundation of Technology will only collect personal data about you that is relevant to the type of transaction or project you have engaged in with us.
For example, we may receive and retain personal information about you when you contact Children's Foundation of Technology to make a donation or when you telephone, email, or write to us, or engage with us via social media channels. In each of these cases, the information we collect is relevant to the type of transaction you are entering into. Data such as your name email or postal address, telephone or mobile number will be necessary both to execute these transactions as well as to enable us contact you for further engagement. Bank account or credit card details will be necessary to process any donations. Age and travel restrictions will be relevant if you are signing up for an international build.

We do not collect your "sensitive personal data" (e.g., health or dietary information).
 </p>
                <p> The Children's Foundation of Technology will use your personal information for the following purposes:  </p>
                <ul>
                    <li> “Service administration”, which means that Children's Foundation of Technology may contact you for reasons related to administering any donations you have made, the completion of commercial or other transactions you have entered into with us, or the activity or online content you have signed up for; </li>
                    <li> To confirm receipt of donations (unless you have asked us not to do this), and to say thank you and provide details of how your donation might be used;</li>
                    <li> In relation to correspondence you have entered into with us whether by letter, email, text, social media, message board or any other means, and to contact you about any content you provide; </li>
                    <li> For internal record keeping so as to keep a record of your relationship with us; </li>
                    <li> To implement any instructions you give us to with regard to withdrawing consent to send marketing information; </li>
                    <li> To use IP addresses to identify the location of users, to block disruptive use and to establish the number of visits from different countries. </li>
                    <li> To analyze and improve the activities and content offered by the Children's Foundation of Technology website to provide you with the most user-friendly navigation experience. We may also use and disclose personal information in aggregate (so that no individuals are identified) for marketing and strategic development purposes. </li>
                </ul>
                <p>  </p>
                <p>  </p>
                <p>  </p>
                <p>  </p>
                <p>  </p>
                <p>  </p>
                <p>  </p>
        		<NavBar />
        	</div>
        )
    }
}

export default PP; 