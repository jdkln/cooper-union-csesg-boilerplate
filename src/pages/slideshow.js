import React, {Component} from 'react';
import ImageGallery from 'react-image-gallery';

import mendyCamera from '../assets/imag/0.jpg';
import graduation from '../assets/imag/2.jpg';
import boyComputer from '../assets/imag/4.jpg';
import constructionPeople from '../assets/imag/8.jpg';
import singer from '../assets/imag/ss2.png';

class Slideshow extends React.Component {
	render() {
    const images = [
		    {
			original: mendyCamera,
			thumbnail: mendyCamera,
		    },
		    {
			original: singer,
			thumbnail: singer,
		    },
		    {
			original: boyComputer, 
			thumbnail: boyComputer,
		    },
		    {
			original: graduation, 
			thumbnail: graduation,
		    },
		    {
			original: constructionPeople,
			thumbnail: constructionPeople,
		    },
    ];
	return (

		<div id = "imageGallery" style= {{marginTop: 60, marginBottom: 60, width: '50%', height: '50%'}}>

			<ImageGallery items={images} />
		
		</div>
		);
	}
    }


export default Slideshow
 
