import React from 'react';
import Header from './Header';
import Grid from '@material-ui/core/Grid';
import TopBar from './topbar';
import Navbar from './navbar';

class News extends React.Component{

	render()
	{
		return(
			<div>
			<Header />
			<TopBar />
<container spacing={24} >
        <Grid item xs={12} style={{marginRight:50, marginLeft:50}}> 
			<h1>
			BREAKING GOOD! AN ARTICLE FROM MRS. KATHERINE TO MR. MENDY KANU AND SCHOOL
			</h1>

			<p> https://medium.com/@katherinecassidy/breaking-good-289c965f542a </p>

			<p> Breaking Good </p>

			<p> He teaches teens computer skills in Sierra Leone — so they don’t have to crush stones for a living, like their elders do. </p>

			<p> By Katherine Cassidy (katherine.cassidy@gmail.com) </p>

			<p> Mendy Kanu lives high on a hillside in Freetown, Sierra Leone — so high that you can’t see bustling, chaotic eastern portion of Freetown when you look down. Fitbit told me I had climbed the equivalent of 43 flights when I went to find him last week. </p>

			<p>  It’s barren where Kanu lives, and he didn’t grow up there. But he belongs there now because he is needed, as more than 200 impoverished teenagers could tell you. That’s how many have passed through his computer skills program since 2014, when he finally had a reason to pay it forward. Two years earlier, he had connected online with a friendly American teacher, who realized that Kanu had abilities and a passion to change others’ lives. The teacher, Adam Beard of Cincinnati, provided Kanu with outside support. As a result, Kanu today helps isolated young men and women learn skills so they might earn a living inside — via the Internet.
No wonder Kanu is always online: He stays in the small, single-story building that serves as a classroom, a computer lab and his living space. The five laptops, a combination of fairly new Macs and PCs, are too valuable to lose. Because if they were stolen from the Children’s Foundation for Technology, as this program is called, dreams of better lives would go out the window, too. </p>

			<p> This is Mendy Kanu’s life, all 26 years of it. He is committed to this community, called Koya Town, because he has little else but a vision. Both of his parents have died, his father during Sierra Leone’s decade-long civil war that ended in 2002. He was fortunate to finish high school in Freetown, even though he was orphaned at 15. He was fortunate to learn computer skills along the way, thanks to Adam Beard’s encouragement. And now he inspires dozens upon dozens of teenagers who otherwise don’t have these chances to learn — because they were born into generations of stone crushers. </p>

			<p> To reach the Children’s Foundation of Technology, first you arrive at PMB Junction along East Freetown’s main highway. You pass through the Wellington Industrial Estate, then walk up, up, up. When you pass the police station, you wonder how much longer you will be walking, because it’s hot and dry. It’s also village-like, so removed from the noise, grime and congestion of Freetown below. Up here, the residents have everything they need: a primary school, a secondary school, a church, a mosque. There are some small shops for daily necessities. Goats and chickens run loose. </p>

			<p> You get to stop when you reach the chief’s house for this isolated community far beyond the modest landscape called Koya Town. Because I’m a visitor, it’s necessary to greet the chief, even if he’s wearing only a ratty T-shirt and watching the world from his veranda. We do that, and photos are part of the greeting. Kanu always has his iPhone handy to snap and record every bigger-than-life moment and action related to CFOT. </p>

			<p> When I see we are coming closer to Kanu’s place, I see that the students have prepared a banner to welcome me. It’s red and blue letters stenciled onto a sheet: THE CHILDREN’S FOUNDATION OF TECHNOLOGY SCHOOL IN SIERRA LEONE (MOTTO: FUTUR HEROES) WELCOME KATHERINE CASSIDY. “TOGETHER WE CAN MAKE A DIFFERENCE.” It touches my heart, and I know I love these kids already. And we haven’t actually met yet, much less reached the place where Mendy Kanu helps them dream big for better lives.
Beyond the banner, we enter into a wide, open space. It’s free of trees, but covered with red dirt, dust, boulders and big rocks. Look around, and you wonder how far the students have to walk back down the hill to reach a water source. </p>

			<p> All of the adults who live in Koya spend their days crushing the big rocks into smaller rocks. It’s an unending process, and everybody does it. The strongest men fill large saucers of fist-sized rocks and balance them on their heads to carry to a waiting pick-up truck. At the end of the day, the truck supervisor will give out a single bill for 10,000 leones — about $1.50. Then that gets divided three ways, for the three men who helped to crush the rocks that day, to fill the truck bed.
This is exactly the life that Kanu wants none of his students to know any longer than they have to. But on this Koya hillside, it’s all there is for anyone’s future: either soul-crushing stone crushing, or the computer skills that Kanu teaches to these teenagers. </p>

			<p> I visited in mid-February, and learn that 32 students have been enrolled in this latest cohort, which runs from September to May. They are all ages — some 12 or 13, others 17 or 18. More than 200 teenagers have passed through the doors of Children’s Foundation of Technology in the last four years. What they have in common is that they don’t attend traditional junior or secondary school — because their families can’t afford it. Some live with guardians, some with single parents, others are orphaned. And three or four of them every night share the sleeping space with Kanu. Because if they didn’t, they wouldn’t have a roof over their heads. </p>

			<p> But they have a place to go to further their futures. At Kanu’s computer space, the students take turns showing up on alternate days, because 32 at once is too many for hands-on time on the five laptops. Once there, they are fed with the easiest meal around — a boiled egg; bread slathered with a choice of margarine or mayonnaise; and a flavored drink mixed from a packet. Attendance has been up, ever since he started serving lunch. </p>

			<p> A free lunch wasn’t always part of the program. The funding for the lunches comes courtesy of another international connection, the students of Campobello Island Consolidated School in New Brunswick, Canada. There, teacher Gina Denbow managed to connect online with Kanu about six years ago, when they were both exploring Internet technology in classrooms through iEARN, or, the International Education and Resource Network. Ever since Kanu and Denbow connected, the students in their respective classrooms have maintained online relationships. So much so that, in 2016, the two groups collaborated on a self-published book (put together in Campobello) about the students’ daily lives in their extraordinarily opposite locations — a fishing village on the eastern edge of Canada, and on a hilltop in western Africa where crushing stones is the only livelihood their families know. </p>

			<p> When I visited, I arrived with extra copies of this book (because I live near the New Brunswick school). The newer, younger students couldn’t wait to read the personal stories of their classmates. The older ones — some of them former students who have transitioned into computer assistants — grinned when they saw their own stories in print all over again. </p>

			<p> Nobody wants to leave Kanu’s side, because he makes learning fun. He provides hope, alongside a daily meal. Children who aren’t yet teenagers also make the place their own, too, and marvel about how the Internet connects all of us. Kanu teaches the teenagers advanced skills such as video editing, and making documentaries for YouTube. Everyone is enthusiastic, and realistic, too: The space doubles as a classroom for math and English skills. </p>

			<p> It’s already crowded, but Kanu knows that bigger space is in the program’s future. With a five-year plan, the fundraising that Adam Beard’s school does annually will make possible the building of a new campus that isn’t so remote and high on a hill. Last October, the CFOT broke ground on land in Waterloo, a city adjacent to Freetown. It will serve a different poverty-ridden neighborhood of young, inquiring minds, all eager to learn all they can about the world beyond Freetown, Sierra Leone. </p>

			<p> Because wherever Kanu’s teaching takes place, students who commit to the program, which is entirely free, gain a renewed and refreshed outlook on all that’s possible for them. Because, they know that they can do things in life that are much bigger than breaking rocks, just to make them smaller. They are already immersed in making their worlds bigger, one laptop lesson at a time. </p>

			<br/>
			<img src= "https://cfotsl.weebly.com/uploads/2/7/1/1/27111981/published/18557354-1244048415705412-7143705597874132969-n.jpg?1499735515" style={{width: 510, height: 320}}/>
			<p style={{fontSize: 12}}> Mr. Kanu sharing food stuffs to the kids at The Children Foundation Of Technology School. Thanks to Mrs. Wendy Williams at Wyoming School for supporting the kids with foods in their last days in school, also thanks to Mr. Beard and the entire Wyoming City Schools. </p>

			    </Grid>
	                </container>

	            <Navbar />
			</div>

			)
	}
}

export default News;