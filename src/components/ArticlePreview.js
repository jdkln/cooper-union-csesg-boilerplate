import React from 'react';
import { HashLink } from 'react-router-hash-link';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';



class ArticlePreview extends React.Component{

	render()
	{
		
		let attachmentNum = "";

		if (Object.keys(this.props.post.attachments).length > 0) {
			attachmentNum = Object.values(this.props.post.attachments)[0];
		}

		//console.log(Object.keys(this.props.post.attachments))

		if(this.props.post)
		{
			return(
					<div id = "blogContent" style = {{}}>

						<Card style = {{maxWidth: 745, margin: ' auto', marginBottom: 10}}>
							<CardMedia style = {{width: '100%', margin: 'auto'}}>
								{this.props.post.featured_image ? 
									(
										<img
											src = {this.props.post.featured_image}
											style = {{width: '60%', margin: '0 150px 0 150px'}}
										/>
									)
									:
									(
										""
									)
								}
							
								{this.props.post.attachments ? 
									(
										<img
											src = {attachmentNum.URL}
											style = {{width: '60%', margin: '0 150px 0 150px'}}
										/>
									)
									:
									(
										""
									)
								}
							</CardMedia>

							<CardContent>
								<h1 style = {{textAlign: 'center'}} dangerouslySetInnerHTML={{ __html: this.props.post.title }}></h1>							

								

								<div style = {{textAlign: 'center'}} dangerouslySetInnerHTML={{ __html: this.props.post.excerpt }}></div>
							</CardContent>

							<CardActions style = {{justifyContent: 'center'}}>
								<HashLink to={ "/blog/" + this.props.post.ID} style = {{margin: '0 150px 0 300px;'}}>
									<button>Read More</button>
								</HashLink>
							</CardActions>
						</Card>
					</div>
				)
		}
		else
		{
			return null;
		}
	}
}

export default ArticlePreview;